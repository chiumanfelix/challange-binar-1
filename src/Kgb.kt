import java.awt.Choice

fun Kgb(){
    val option = arrayOf("rock", "paper", "scissors")
    val computerChoice = getComputerChoice(option)
    val playerChoice = getPlayerChoice(option)
    printResult(playerChoice, computerChoice)
}

fun getComputerChoice(optionsParam: Array<String>) =
    optionsParam[(Math.random()*optionsParam.size).toInt()]

fun getPlayerChoice(optionsParam: Array<String>): String{
    var isValidChoice = false
    var playerChoice = ""

    while (!isValidChoice){
        println("Please Choose any of the following: ")
        for(item in optionsParam)
            print(" $item")
        println(" : ")

        val playerInput = readLine()?.lowercase()

        if(playerInput != null && playerInput in optionsParam){
            isValidChoice = true
            playerChoice = playerInput
        }

        else if(!isValidChoice){
            println("Invalid choice \n")
        }
    }
    return playerChoice
}

fun printResult(playerChoice: String, computerChoice: String){
    val result: String
    if (playerChoice == computerChoice){
        result = "Tie!!"
    }
    else if ((playerChoice == "scissors" && computerChoice == "paper")||
        (playerChoice == "paper" && computerChoice == "rock")||
        (playerChoice == "rock" && computerChoice == "scissors")){
        result = "You Win"
    }
    else{
        result = "You Lose"
    }
    println("$result \n")
}